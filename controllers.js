var mysql = require('mysql');

var world_x_connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'cuchara1',
  database : 'world_x'
});

function executesRetrieveListing(req, res, connection, queryString, params) {
  connection.query(queryString, params, function (error, results, fields) {
    if (error) {
      console.error('error queryng: ' + queryString + ' :: '  + error + ': ' +error.stack);
      throw error;
    } else {
      res.json(results);
    }
  });
}

module.exports = {
  world_x_connection: world_x_connection,

  getCountriesList: function(req, res) {
    executesRetrieveListing(
      req,
      res,
      world_x_connection,
      'SELECT Name AS name, Code AS code FROM country ORDER BY Name ASC',
      []);
  },

  getDistrictsFromCountry: function(req, res) {
    executesRetrieveListing(
      req, res, world_x_connection,
      'SELECT DISTINCT District AS name FROM city WHERE CountryCode = ? ORDER BY District ASC',
      [req.params.countryCode]);
  },

  getCitiesFromCountryAndDistrict: function(req, res) {
    executesRetrieveListing(
      req, res, world_x_connection,
      'SELECT Name AS name FROM city WHERE CountryCode = ? AND District = ? ORDER BY Name ASC',
      [req.params.countryCode, req.params.district]);
  },
};