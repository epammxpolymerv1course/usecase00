
const COMPUTATION_STATUS = {
  IDLE : 'IDLE',
  PLAYING : 'PLAYING',
};

Polymer({
  is: "montecarlo-pi-page",
  
  properties: {
    showStats: {
      type: Boolean,
      value: true,
    },

    playing: {
      type: Boolean,
      value: false,
    },

    playButtonLabel: {
      type: String,
      value: COMPUTATION_STATUS.IDLE,
    },

    points: {
      type: Array,
      value: () => [],
      notify: true,
    },

    copyPoints: {
      type: Array,
      value: () => [],
      notify: true,
    },
  },
  

  onStartTapped_(event) {
    this.playing = !this.playing;

    if (this.playing) {
      this.playButtonLabel = COMPUTATION_STATUS.PLAYING;

      this.generatePoints_();

    } else {
      this.playButtonLabel = COMPUTATION_STATUS.IDLE;
    }
  },

  onStatsTapped_(event) {
    this.showStats = !this.showStats;
  },

  onCopyTapped_(event) {
    const newPoints = [];

    for (point of this.points) {
      newPoints.push(point);
    }

    this.copyPoints = newPoints;
  },

  onClearTapped_(event) {
    this.points = [];
  },

  generatePoints_() {
    let newPoints = [];
    let j;

    for(j = 0; j < 1000; j++) {
      newPoints.push({x: Math.random(), y: Math.random()});
    }

    this.$.experimentLab.addPoints(newPoints);

    for(j = 0; j < 20000; j++) {
      newPoints.push({x: Math.random(), y: Math.random()});
    }

    this.$.fasterLab.addPoints(newPoints);

    if (this.playing) {
      window.setTimeout(() => this.generatePoints_(), 500);  
    }
  },
});