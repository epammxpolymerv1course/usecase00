const POINT_COLORS = {
  INSIDE: 'red',
  OUTSIDE: 'blue',
};

Polymer({
  is: "montecarlo-pi",
  
  properties: {
    title: String,

    points: {
      type: Array,
      value: () => [],
      observer: 'onPointsChanged_',
      notify: true,
    },

    totalPoints: {
      type: Number,
      value: 0,
    },

    insidePoints: {
      type: Number,
      value: 0,
    },

    piValue: {
      type: String,
      computed: 'computePiValue_(totalPoints, insidePoints)',
    },

    showStats: {
      type: Boolean,
      value: false,
    },
  },
  
  ready() {
    this.renderPoints_(this.points);
  },

  onPointsChanged_(points) {
    this.renderPoints_(points);
    this.doCompute_(points);
  },

  renderPoints_(points) {
    const drawingArea = this.$.drawingArea;
    drawingArea.redrawCanvas();

    for (point of this.points) {
      if (this.insideCircle_(point)) {
        drawingArea.drawPoint(point, POINT_COLORS.INSIDE);
      } else {
        drawingArea.drawPoint(point, POINT_COLORS.OUTSIDE);
      }
    }
  },

  insideCircle_(point) {
    return (point.x * point.x) + (point.y * point.y) <= 1;
  },

  computePiValue_(totalPoints, insidePoints) {
    if (totalPoints == 0) {
      return '';
    }

    return ((insidePoints / totalPoints) * 4).toFixed(4);
  },

  doCompute_(points) {
    let totalPoints = points ? points.length : 0;
    let insidePoints = 0;

    for (point of this.points) {
      if (this.insideCircle_(point)) {
        insidePoints++;
      }
    }

    this.totalPoints = totalPoints;
    this.insidePoints = insidePoints;
  },

  addPoints(otherPoints) {
    let totalPoints = 0;
    let insidePoints = 0;
    const drawingArea = this.$.drawingArea;

    for (point of otherPoints) {
      this.points.push(point);
      totalPoints++;
      
      if (this.insideCircle_(point)) {
        insidePoints++;
        drawingArea.drawPoint(point, POINT_COLORS.INSIDE);
      } else {
        drawingArea.drawPoint(point, POINT_COLORS.OUTSIDE);
      }
    }

    this.totalPoints += totalPoints;
    this.insidePoints += insidePoints;
  },
});
