const CANVAS_DATA = {
  WIDTH: 200,
  HEIGHT: 200,
};

Polymer({
  is: "montecarlo-drawing-area",
  canvasElem: null,
  ctx: null,
    
  ready() {
    this.canvasElem = this.querySelector('canvas');
    this.ctx = this.canvasElem.getContext('2d');
    this.redrawCanvas();
  },

  redrawCanvas() {
    if (!this.ctx || !this.canvasElem) {
      return;
    }

    const ctx = this.ctx;
    //console.log('width: ' + this.canvasElem.width + ', height: ' + this.canvasElem.height);
    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, CANVAS_DATA.WIDTH, CANVAS_DATA.HEIGHT);
    ctx.fill();

    ctx.beginPath();
    // ctx.arc(centerX, centerY, radius, startAngle, endAngle, isAntiClockwise);
    // 1 radian = 57.2957795 degrees
    // radians = (PI/180) * degrees
    ctx.arc(0, CANVAS_DATA.HEIGHT, CANVAS_DATA.HEIGHT, 0, Math.PI/2.0, true);
    ctx.fillStyle = 'rgb(255, 255, 200)';
    ctx.fill();
    ctx.fillStyle = 'black';
    ctx.stroke();
    ctx.closePath();
  },

  drawPoint(point, style) {
    this.ctx.fillStyle = style;
    this.ctx.beginPath();
    this.ctx.arc(
      point.x * CANVAS_DATA.WIDTH,
      CANVAS_DATA.HEIGHT - (point.y * CANVAS_DATA.HEIGHT),
      1,
      0,
      2 * Math.PI,
      false);
    this.ctx.fill();
    this.ctx.closePath();
  },
});
